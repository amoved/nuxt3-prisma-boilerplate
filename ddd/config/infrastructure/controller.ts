import { Config, type SMTPConfig } from "~/ddd/config/domain/config";
import type { ConfigService } from "~/ddd/config/application/config.service";

export class ConfigController {
    private readonly configService: ConfigService;

    constructor(configService: ConfigService){
        this.configService = configService;
    }

    public get(){
        return this.configService.get()
    }

    public getSmtp(){
        return this.configService.getSmtp()
    }

    public async updateSmtp( config: SMTPConfig ): Promise<SMTPConfig | false> {
        return await this.configService.updateSmtp(config)
    }

    public async verifySmtpConnection( smtpConfig: SMTPConfig ): Promise<Boolean> {
        return await this.configService.verifySmtpConnection(smtpConfig)
    }
}