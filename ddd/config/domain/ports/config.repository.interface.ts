import type { ConfigObject } from "../config";

export interface IConfigRepository {
    create( name: string, value: string | boolean ): Promise<boolean>;
    update( name: string, value: string | boolean ): Promise<boolean>;
    getAll(): Promise<ConfigObject>;
    getByName( name: string ): Promise<string | false>;
}