import { DomainEventEmitter } from '~/ddd/shared/domain/DomainEventEmitter';
import type { SMTPConfig } from './config';

// Define all events names and input params
type EventMap = {
    'SMTP_UPDATED': [smtp: SMTPConfig],
}

export const configDomainEventEmitter = new DomainEventEmitter<EventMap>()