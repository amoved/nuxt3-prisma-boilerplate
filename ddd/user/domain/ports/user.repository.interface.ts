import type { User, Status, Role } from '../user'

export interface IUserRepository {
    register(name: string, email: string, passwordHash: string): Promise<User | false>;
    getUserById(id: string): Promise<User | false>;
    getUserByEmail(email: string): Promise<User | false>;
    getUserByTemporaryAccessToken(temporaryAccessToken: string): Promise<User | false>;
    setUserStatus(id: string, status: Status): Promise<User | false>;
    setUserRole(id: string, role: Role): Promise<User | false>;
    setPassword(id: string, passwordHash: string): Promise<User | false>;
    setVerified(id: string): Promise<User | false>;
    setPendingPasswordRestore(id: string, pending: boolean): Promise<User | false>;
    setTemporaryAccessToken(id: string, temporaryAccessToken: string): Promise<User | false>;
}