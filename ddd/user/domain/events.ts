import { DomainEventEmitter } from '~/ddd/shared/domain/DomainEventEmitter';
import type { User } from "./user"

// Define all events names and input params
type EventMap = {
    'USER_CREATED': [user: User],
    'USER_VERIFIED': [user: User],
    'USER_RECOVER': [user: User],
    'USER_LOGGED_IN': [user: User],
    'USER_LOGGED_OUT': [user: User],
    'USER_UPDATED': [user: User],
    'USER_DELETED': [user: User],
}

export const userDomainEventEmitter = new DomainEventEmitter<EventMap>()