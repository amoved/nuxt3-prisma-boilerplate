import type { IUserRepository } from "../domain/ports/user.repository.interface";
import { Log, LogType } from "../../log/domain/log";
import type { Role, Status, User } from "../domain/user";
import { mapDomainToPrismaStatus, mapPrismaToDomainRole, mapPrismaToDomainStatus } from "./prisma.utils";
import prisma, { Prisma } from "~/prisma";
import type { ILoggerPort } from "~/ddd/log/application/logger.port.interface";

export class UserRepositoryPrismaMySQL implements IUserRepository {

    private loggerPort

    constructor(loggerPort: ILoggerPort) {
        this.loggerPort = loggerPort
    }

    public async register(name: string, email: string, passwordHash: string): Promise<User | false> {
        try {
            const user = await prisma.user.create({
                data: {
                    name,
                    email,
                    passwordHash,
                }
            })
            return {
                ...user,
                status: mapPrismaToDomainStatus(user.status),
                role: mapPrismaToDomainRole(user.role)
            }
        } catch(e) {
            if(e instanceof Prisma.PrismaClientKnownRequestError){
                // Handle e-mail exists
                if(e.code === 'P2002') {
                    this.loggerPort.error("Tried to register with an already existing email.")
                    // return new Log(LogType.INFO, $t("register.recover"), ErrorCodes.registerEmail)
                }
            } else {
                // Unknown error
                if(e instanceof Object && Object.hasOwn(e, 'message')){
                    this.loggerPort.error(e.message)
                } else {
                    this.loggerPort.error('Unhandled exception error at "register" action.')
                }
            }
            
        }
        return false
    }

    public async getUserById(id: string): Promise<User | false> {
        try {
            const user = await prisma.user.findUnique({
                where: {
                    id
                }
            })
            return {
                ...user,
                status: mapPrismaToDomainStatus(user.status),
                role: mapPrismaToDomainRole(user.role)
            }
        } catch(e) {
            if(e instanceof Prisma.PrismaClientKnownRequestError){
                // Handle e-mail exists
                if(e.code === 'P2002') {
                    this.loggerPort.error("Tried to register with an already existing email.")
                    // return new Log(LogType.INFO, $t("register.recover"), ErrorCodes.registerEmail)
                }
            } else {
                // Unknown error
                if(e instanceof Object && Object.hasOwn(e, 'message')){
                    this.loggerPort.error(e.message)
                } else {
                    this.loggerPort.error('Unhandled exception error at "register" action.')
                }
            }
            
        }
        return false
    }

    public async getUserByEmail(email: string): Promise<User | false> {
        const user = await prisma.user.findUnique({
            where: {
                email
            }
        })
        this.loggerPort.debug(`[UserRepository: getUserByEmail] Found user: ${user.id}`)
        return !!user ? user : false
    }

    public async getUserByTemporaryAccessToken(temporaryAccessToken: string): Promise<User | false> {
        
        try {
            const user = await prisma.user.findFirst({
                where: {
                    temporaryAccessToken
                }
            })
            this.loggerPort.debug(`[UserRepository: getUserByTemporaryAccessToken] Found user: ${user.id}`)
            return {
                ...user,
                status: mapPrismaToDomainStatus(user.status),
                role: mapPrismaToDomainRole(user.role)
            }
        } catch(e) {
            if(e instanceof Prisma.PrismaClientKnownRequestError){
                // Handle e-mail exists
                if(e.code === 'P2002') {
                    this.loggerPort.error("Tried to register with an already existing email.")
                    // return new Log(LogType.INFO, $t("register.recover"), ErrorCodes.registerEmail)
                }
            } else {
                // Unknown error
                if(e instanceof Object && Object.hasOwn(e, 'message')){
                    this.loggerPort.error(e.message)
                } else {
                    this.loggerPort.error('Unhandled exception error at "register" action.')
                }
            }
            
        }
        return false
    }

    public async setUserStatus(id: string, status: Status): Promise<User | false> {
        let user = await prisma.user.update({
            where: {
                id
            },
            data: {
                status: mapDomainToPrismaStatus(status)
            }
        })
        console.log(user)
        return user
    }

    public async setUserRole(id: string, role: Role): Promise<User | false> {
        // let user = await prisma.user.update({
        //     where: {
        //         id
        //     },
        //     data: {
        //         status: mapDomainToPrismaStatus(status)
        //     }
        // })
        // console.log(user)
        // return user
    }

    public async setPassword(id: string, passwordHash: string): Promise<User | false>{
        try {
            const user = await prisma.user.update({
                where: {
                    id
                },
                data: {
                    passwordHash
                }
            })
            return {
                ...user,
                status: mapPrismaToDomainStatus(user.status),
                role: mapPrismaToDomainRole(user.role)
            }
        } catch(e) {
            if(e instanceof Prisma.PrismaClientKnownRequestError){
                // Handle e-mail exists
                if(e.code === 'P2002') {
                    this.loggerPort.error("Tried to register with an already existing email.")
                    return new Log(LogType.INFO, $t("register.recover"), ErrorCodes.registerEmail)
                }
            } else {
                // Unknown error
                if(e instanceof Object && Object.hasOwn(e, 'message')){
                    this.loggerPort.error(e.message)
                } else {
                    this.loggerPort.error('Unhandled exception error at "register" action.')
                }
            }
            
        }
        return false
    }

    public async setVerified(id: string): Promise<User | false>{
        try {
            const user = await prisma.user.update({
                where: {
                    id
                },
                data: {
                    verified: true
                }
            })

            return {
                ...user,
                status: mapPrismaToDomainStatus(user.status),
                role: mapPrismaToDomainRole(user.role)
            }
        } catch(e) {
            if(e instanceof Prisma.PrismaClientKnownRequestError){
                // Handle e-mail exists
                if(e.code === 'P2002') {
                    this.loggerPort.error("Tried to register with an already existing email.")
                    return new Log(LogType.INFO, $t("register.recover"), ErrorCodes.registerEmail)
                }
            } else {
                // Unknown error
                if(e instanceof Object && Object.hasOwn(e, 'message')){
                    this.loggerPort.error(e.message)
                } else {
                    this.loggerPort.error('Unhandled exception error at "register" action.')
                }
            }
            
        }
        return false
    }

    public async setPendingPasswordRestore(id: string, pending: boolean): Promise<User | false>{
        try {
            const user = await prisma.user.update({
                where: {
                    id
                },
                data: {
                    pendingPasswordRestore: pending
                }
            })

            return {
                ...user,
                status: mapPrismaToDomainStatus(user.status),
                role: mapPrismaToDomainRole(user.role)
            }
        } catch(e) {
            if(e instanceof Prisma.PrismaClientKnownRequestError){
                // Handle e-mail exists
                if(e.code === 'P2002') {
                    this.loggerPort.error("Tried to register with an already existing email.")
                    return new Log(LogType.INFO, $t("register.recover"), ErrorCodes.registerEmail)
                }
            } else {
                // Unknown error
                if(e instanceof Object && Object.hasOwn(e, 'message')){
                    this.loggerPort.error(e.message)
                } else {
                    this.loggerPort.error('Unhandled exception error at "register" action.')
                }
            }
            
        }
        return false
    }

    public async setTemporaryAccessToken(id: string, temporaryAccessToken: string): Promise<User | false>{
        try {
            const user = await prisma.user.update({
                where: {
                    id
                },
                data: {
                    temporaryAccessToken
                }
            })

            return {
                ...user,
                status: mapPrismaToDomainStatus(user.status),
                role: mapPrismaToDomainRole(user.role)
            }
        } catch(e) {
            if(!(e instanceof Prisma.PrismaClientKnownRequestError)){
                // Unknown error
                if(e instanceof Object && Object.hasOwn(e, 'message')){
                    this.loggerPort.error(e.message)
                } else {
                    this.loggerPort.error('Unhandled exception error at "register" action.')
                }
            }
            
        }
        return false
    }
}