import { Role, Status } from "../domain/user";
import { Status as PrismaStatus, Role as PrismaRole } from "@prisma/client";

export function mapDomainToPrismaStatus(status: Status) {
    switch(status){
        case Status.LOGGED_IN:
            return PrismaStatus.LOGGED_IN 
        case Status.LOGGED_OUT:
            return PrismaStatus.LOGGED_OUT 
    }
} 

export function mapPrismaToDomainStatus(status: PrismaStatus) {
    switch(status){
        case PrismaStatus.LOGGED_IN:
            return  Status.LOGGED_IN
        case PrismaStatus.LOGGED_OUT:
            return Status.LOGGED_OUT 
    }
} 

export function mapDomainToPrismaRole(status: Role) {
    switch(status){
        case Role.USER:
            return PrismaRole.USER 
        case Role.ADMIN:
            return PrismaRole.ADMIN 
    }
} 

export function mapPrismaToDomainRole(status: PrismaRole) {
    switch(status){
        case PrismaRole.USER:
            return  Role.USER
        case PrismaRole.ADMIN:
            return Role.ADMIN 
    }
} 