import { User } from "~/ddd/user/domain/user";
import { UserService } from "~/ddd/user/application/user.service";

export class UserController {
    private readonly userService: UserService;

    constructor(userService: UserService){
        this.userService = userService;
    }

    public async register( name: string, email: string, password: string ): Promise<User | false> {
        return await this.userService.register(name, email, password)
    }

    public async verify( temporaryAccessToken: string ): Promise<boolean> {
        return await this.userService.verify(temporaryAccessToken)
    }

    public async login( email: string, password: string ): Promise<User | false> {
        return await this.userService.login(email, password)
    }

    public async session(authorization: string): Promise<User | false> {
        return await this.userService.session(authorization)
    }

    public async logout(authorization: string): Promise<boolean> {
        return await this.userService.logout(authorization)
    }

    public async recover( email: string ): Promise<User | false> {
        return await this.userService.recover(email)
    }

    public async restore( temporaryAccessToken: string, password: string ): Promise<User | false> {
        return await this.userService.restore(temporaryAccessToken, password)
    }
}