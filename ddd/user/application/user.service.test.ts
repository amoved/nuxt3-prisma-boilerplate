import { AuthAdapterBcryptJWT } from './../../shared/infrastructure/adapter.bcryptJwt';
import { LoggerAdapterWinston } from '~/ddd/log/infrastructure/adapter.winston'
import { UserService } from './user.service'
import { expect, describe, it } from 'vitest'
import { config } from '~/server/config'
import { UserRepositoryPrismaMySQL } from '../infrastructure/repository.mysqlPrisma'


describe('Register use case', () => {

    // Config service
    const loggerPort = new LoggerAdapterWinston(config)
    const authPort = new AuthAdapterBcryptJWT()
    const userRepository = new UserRepositoryPrismaMySQL(loggerPort)
    const userService = new UserService(userRepository,authPort,loggerPort) 

    it('Should reject invalid input', () => {
        const name: number = 4
        const email: string = 'yayo@mail.com'
        const password: string = 'laContraseni4'
        expect(userService.register(name, email, password)).toBe(false)
    })
})