import { UserRepositoryPrismaMySQL } from '~/ddd/user/infrastructure/repository.mysqlPrisma';
import { User, Status, Role } from '../domain/user'
import { type IUserRepository } from "../domain/ports/user.repository.interface";
import type { IAuthPort } from '~/ddd/shared/application/ports/auth.port.interface';
import type { ILoggerPort } from '~/ddd/log/application/ports/logger.port.interface';
import { userDomainEventEmitter } from '../domain/events';

export class UserService {
    private loggerPort: ILoggerPort
    private userRepository: IUserRepository
    private authPort: IAuthPort

    constructor(userRepository: IUserRepository, authPort: IAuthPort, loggerPort: ILoggerPort){
        this.loggerPort = loggerPort
        this.userRepository = userRepository
        this.authPort = authPort
    }

    // Registers a user account that will need to be activated trhough e-mail.
    public async register(name: string, email: string, password: string): Promise<User | false>{

        this.loggerPort.debug("[User: register] User registration request")

        const passwordHash = await this.authPort.hashPassword(password)

        let user = await this.userRepository.register(name, email, passwordHash)
        const payload = { id: user.id }
        const temporaryAccessToken = this.authPort.generateAccessToken(payload,'2h')
        user = await this.userRepository.setTemporaryAccessToken(user.id,temporaryAccessToken)

        // Log results
        if(!!user){
            this.loggerPort.info(`[User: register] User ${user.id} successfuly created`)
            userDomainEventEmitter.emit('USER_CREATED', user)
            this.loggerPort.debug("[USER_CREATED] Domain event emitted!!")
        } else {
            this.loggerPort.error("[User: register] User registration failed")
        }

        return user
    }

    public async verify(temporaryAccessToken: string): Promise <boolean>{

        this.loggerPort.info(`[User: verify] Start verification process for ${temporaryAccessToken}`)
        
        try {
            // Check access token on db and its validity
            let user = await this.userRepository.getUserByTemporaryAccessToken(temporaryAccessToken)
            this.loggerPort.info(`Found user with id: ${user.id}`)
            
            const tokenData = this.authPort.verifyAccessToken(temporaryAccessToken)
            
            this.loggerPort.info(`[User: verify] User to verify: ${tokenData.id}`)
            this.loggerPort.info(`[User: verify] Token: ${temporaryAccessToken}`)
            
            // If valid token
            if(!!user && tokenData.id){
                user = await this.userRepository.getUserById(tokenData.id)
                // If user exists in DB
                if(!!user){
                    // Clear temporary access token
                    user = await this.userRepository.setTemporaryAccessToken(user.id,'')
                    const updatedUser = await this.userRepository.setVerified(user.id)
                    userDomainEventEmitter.emit('USER_VERIFIED', updatedUser)
                    return true
                }
            }
        } catch(e) {
            this.loggerPort.error("[User: verify] Verification process failed")
            this.loggerPort.error(e)
        }
        this.loggerPort.info(`[User: verify] No user found or access token is invalid`)


        return false
    }

    public async recover(email: string): Promise <boolean>{

        this.loggerPort.debug("[User: recover] User recover request")

        // Find user by email
        let user = await this.userRepository.getUserByEmail(email)
        // Generate link
        const payload = { id: user.id }
        const temporaryAccessToken = await this.authPort.generateAccessToken(payload,'2h')
        user = await this.userRepository.setTemporaryAccessToken(user.id,temporaryAccessToken)
        // Set pending password restore
        user = await this.userRepository.setPendingPasswordRestore(user.id, true)

        if(!!user){
            userDomainEventEmitter.emit('USER_RECOVER', user)
            this.loggerPort.debug("[USER_RECOVER] Domain event emitted!!")
            return true
        }

        return false
    }

    public async restore(temporaryAccessToken: string, password: string): Promise<User | false> {

        try {
            // Check access token on db and its validity
            let user = await this.userRepository.getUserByTemporaryAccessToken(temporaryAccessToken)
            const tokenData = this.authPort.verifyAccessToken(temporaryAccessToken)
            
            if(!!user && tokenData.id){
                // Set new password
                const passwordHash = await this.authPort.hashPassword(password)
                user = await this.userRepository.setPassword(user.id, passwordHash)
                // Clear temporary access token and pendnig password restore
                user = await this.userRepository.setTemporaryAccessToken(user.id, '')
                user = await this.userRepository.setPendingPasswordRestore(user.id, false)

                return user
            }

        } catch(e) {
            this.loggerPort.error(e)
        }

        throw createError({
            statusCode: 403,
            statusMessage: "[User: restore] Not authorized",
        });

        return false

    }

    public async login(email: string, password: string): Promise<string> {

        // Check if user exists
        let user = await this.userRepository.getUserByEmail(email)
        
        // Check if user is verified
        if(user && user.verified){
            try {
                // Check if password is correct
                const authenticated = await this.authPort.testPassword(password, user.passwordHash)
                // Set LOGGED_IN status
                user = await this.userRepository.setUserStatus(user.id, Status.LOGGED_IN)
                
                if ( !!!user ) {
                    throw createError({
                      statusCode: 403,
                      statusMessage: "[User: login] Not authorized",
                    });
                } else {
                    this.loggerPort.debug(`[User: login] ${user.id} logged in.`)
                } 
                
                // Get temporary access token
                const accessToken = await this.authPort.generateAccessToken({id: user.id})

                return accessToken
            } catch (error) {
                this.loggerPort.error(error)
                throw(error)
            }
        }

        throw createError({
            statusCode: 403,
            statusMessage: "[User: login] User doesn't exist or is not verified",
        });

        return false

    }

    public async session(authorization: string): Promise<User | false> {
        const tokenData = this.authPort.verifyAccessToken(authorization)
        if(tokenData.id){
            return await this.userRepository.getUserById(tokenData.id)
        }
        return false
    }

    public async logout(authorization: string): Promise<boolean> {
        const tokenData = this.authPort.verifyAccessToken(authorization)
        if(tokenData.id){
            const user = await this.userRepository.setUserStatus(tokenData.id, Status.LOGGED_OUT)
            this.loggerPort.debug(`[User: logout] ${user.id} logged out.`)
            return !!user
        }
        return false
    }
}