import { configDomainEventEmitter } from "~/ddd/config/domain/events";
import { Notifier } from "../domain/notifier";
import type { IMailerPort, MailerOptions } from "../domain/ports/mailer.interface"
import type { SMTPConfig, SmtpConnection } from '../domain/smtpConnection';
import { userDomainEventEmitter } from '~/ddd/user/domain/events';
import type { User } from "~/ddd/user/domain/user";

export class MailerService {
    
    private mailerPort: IMailerPort
    private options: MailerOptions
    private baseUrl: string

    constructor(mailerOptions: MailerOptions, mailerPort: IMailerPort){
        this.options = mailerOptions
        this.mailerPort = mailerPort
        this.baseUrl = 'http://localhost:3000'

        // Bind this to the function context
        this.notifyUserCreated = this.notifyUserCreated.bind(this)
        this.notifyUserPasswordRecovery = this.notifyUserPasswordRecovery.bind(this)

        // User service events
        userDomainEventEmitter.on('USER_CREATED', this.notifyUserCreated)
        console.log("Mailer suscribed to USER_CREATED event")

        userDomainEventEmitter.on('USER_RECOVER', this.notifyUserPasswordRecovery)
        console.log("Mailer suscribed to USER_CREATED event")

        configDomainEventEmitter.on('SMTP_UPDATED', this.updateConfig)
        console.log("Mailer suscribed to SMTP_UPDATED event")
    }

    public async verifySmtpConnection(options: SmtpConnection): Promise<boolean>{
        return await this.mailerPort.verifySmtpConnection(options)
    }

    public updateNotifier(notifier: Notifier){
        return this.mailerPort.updateNotifier(notifier)
    }

    public updateSmtpConfig(config: SMTPConfig) {
        return this.mailerPort.updateSmtpConfig(config)
    }

    public async send(addresses: string[], template: string, locale: string, params: object = {}) {
        return await this.mailerPort.send(addresses, template, locale, params)
    }

    private updateConfig(config: SMTPConfig) {
        console.log(`[Mailer] Updating SMTP config`)
        this.updateNotifier(new Notifier(this.options.notifier.fromName, config.email, this.options.notifier.locale))
        this.updateSmtpConfig(config)
    }

    private async notifyUserCreated(user: User): Promise<void> {
        console.log(`[Mailer] On my way to welcome ${user.name}`)
        try {
            const verificationLink = `${this.baseUrl}/verify?code=${user.temporaryAccessToken}`
            await this.mailerPort.send([user.email],'welcome',user.locale,{userName: user.name,verificationLink})
        } catch (e) {
            console.log(e)
        }
        
    }

    private async notifyUserPasswordRecovery(user: User):Promise<void> {
        console.log(`[Mailer] Sending user recovery link to ${user.name}`)
        try {
            const recoveryLink = `${this.baseUrl}/recover?code=${user.temporaryAccessToken}`
            await this.mailerPort.send([user.email],'recover',user.locale,{recoveryLink})
        } catch (e) {
            console.log(e)
        }
    }


}