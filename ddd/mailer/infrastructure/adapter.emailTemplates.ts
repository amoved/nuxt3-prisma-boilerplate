import { transports } from 'winston';
import type { SMTPConfig, SmtpConnection } from '../domain/smtpConnection';
import { Notifier } from '../domain/notifier';
import type { IMailerPort, MailerOptions } from '../domain/ports/mailer.interface';
import Email from 'email-templates'
import path from 'path';


export class MailerAdapterEmailTemplates implements IMailerPort {

    private mailer: any
    private options: any

    constructor(options: MailerOptions) {
  
      this.options = {
        views: { 
          root: path.join(process.cwd(),'server/emails'),
          htmlToText: true,
          juice: true,
          juiceResources: {
            preserveImportant: true,
            webResources: {
              relativeTo: path.join(process.cwd(),'server/emails')
            }
          },
        },
        message: {
          from: `${options.notifier.fromName} <${options.notifier.fromAddress}>`
        },
        preview: false,
        send: true,
        i18n: {
          directory: path.join(process.cwd(), 'locales/email'),
          objectNotation: true,
          syncFiles: false,
          updateFiles: false,
          defaultLocale: options.notifier.locale || 'es',
          // locale,
          locales: ['es','ca','eu','gl','pt','en']
          // locales: Object.keys(locales)
        },
        transport: {
          host: options.smtpConnection.host || process.env.SMTP_HOST,
          port: options.smtpConnection.port || process.env.SMTP_PORT,
          secure: options.smtpConnection.useTLS || process.env.SMTP_USE_TLS,
          auth: {
            user: options.smtpConnection.email || process.env.SMTP_USER,
            pass: options.smtpConnection.password || process.env.SMTP_PASSWORD,
          }
        }
      }

      this.setMailer()
    }

    private setMailer(): boolean {
      try {
        this.mailer = new Email(this.options)
      } catch(e) {
        console.log(e)
        return false
      }
      return true
    }

    public updateNotifier(notifier: Notifier){
      if(notifier.fromName && notifier.fromAddress){
        this.options.message.from = `${notifier.fromName} <${notifier.fromAddress}>`
      }
      if(notifier.locale){
        this.options.i18n.defaultLocale = notifier.locale
      }
      return this.setMailer()
    }

    public updateSmtpConfig(config: SMTPConfig){
      this.options.transport = {
        host: config.host,
        port: config.port,
        secure: config.useTLS,
        auth: {
          user: config.email,
          pass: config.password
        }
      }
      return this.setMailer()
    }

    public async verifySmtpConnection(options: SmtpConnection){
      
      const testMailer = new Email({
        ...this.options,
        preview: false,
        transport: {
          host: options.host,
          port: options.port,
          secure: options.useTLS,
          auth: {
            user: options.email,
            pass: options.password,
          }
        }
      })

      const msg = {
        template: 'smtp',
        message: {
          to: options.email
        },
        locals: {
          // ...locals,
          locale: options.locale || 'es',
          config: {
            title: 'Test de configuración SMTP',
            baseurl: 'https://amoved.es',
            description: 'Mensaje automático de prueba.',
          }
        }
      }

      let response

      try {
        response = await testMailer.send(msg)
      } catch(e) {
        console.log(e)
      }

      return !(response.rejected.length > 0)
    }

    public async send(addresses: string[], template: string, locale: string, params: object = {}) {
      
      locale = locale ? locale : this.options.i18n.defaultLocale

      const msg = {
        template,
        message: {
          to: addresses
        },
        // TODO: remove this mock from here
        locals: {
          locale,
          config: {
            title: 'Mensajería App',
            baseurl: 'https://amoved.es',
            description: 'Mensaje automático.',
            email: 'taller@amoved.es',
            ...params
          }
        }
      }

      try {
        console.log("[Mailer Port] Sending notification")
        console.log(msg)
        await this.mailer.send(msg)
      } catch(e) {
          console.log(e)
          return false          
      }
      return true
    }

}
