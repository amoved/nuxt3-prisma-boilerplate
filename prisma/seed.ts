import { PrismaClient, type Status, type Role } from '@prisma/client'
import bcrypt from 'bcryptjs'

const prisma = new PrismaClient()

const admin: Role = "ADMIN"
const user: Role = "USER"
const status: Status = "LOGGED_OUT"

// Passwords are hashed before sending to DB in main()
const userData = [
  {
    name: 'Luis',
    email: 'luis@amoved.es',
    passwordHash: '12345678',
    role: admin,
    verified: true
  },
  {
    name: 'Carlos',
    email: 'carlos@amoved.es',
    passwordHash: '12345678',
    role: user,
    verified: true
  },
  {
    name: 'Elena',
    email: 'elena@amoved.es',
    passwordHash: '12345678',
    role: user,
    verified: true
  },
]

async function main() {
  console.log(`Start seeding ...`)
  for (const u of userData) {
    let salt = await bcrypt.genSalt(10)
    u.passwordHash = await bcrypt.hash(u.passwordHash,salt)
    const user = await prisma.user.create({
      data: u,
    })
    console.log(`Created user with id: ${user.id}`)
  }
  console.log(`Seeding finished.`)
}

main()
  .then(async () => {
    await prisma.$disconnect()
  })
  .catch(async (e) => {
    console.error(e)
    await prisma.$disconnect()
    process.exit(1)
  })