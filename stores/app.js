// Warning: remember to check if the components are intending to uso 
// the old mutation or the old action, now only old actions are availbale.
// To make old mutations available rename and isolate the state changer line.
// Check the gancio code in Vuex.
import { defineStore } from 'pinia'

// You can name the return value of `defineStore()` anything you want,
// but it's best to use the name of the store and surround it with `use`
// and `Store` (e.g. `useUserStore`, `useCartStore`, `useProductStore`)
// the first argument is a unique id of the store across your application
export const useAppStore = defineStore('useAppStore', {
  state: () => ({
    localSettings: {
      hide_thumbs: null,
      "theme.is_dark": null,
    },
    settings: {
      instance_timezone: "Europe/Rome",
      instance_name: "",
      allow_registration: true,
      allow_anon_event: true,
      enable_federation: false,
      enable_resources: false,
      hide_boosts: true,
      enable_trusted_instances: true,
      trusted_instances: [],
      trusted_instances_label: "",
      "theme.is_dark": true,
    },
  }),
  getters: {
    hide_thumbs(state) {
      return ![true, false].includes(state.localSettings["hide_thumbs"])
        ? state.settings.hide_thumbs
        : state.localSettings.hide_thumbs;
    },
    is_dark(state) {
      return ![true, false].includes(state.localSettings["theme.is_dark"])
        ? state.settings["theme.is_dark"]
        : state.localSettings["theme.is_dark"];
    },
  },
  actions: {
    // this method is called server side only for each request for nuxt
    // we use it to get configuration from db, set locale, etc...
    nuxtServerInit({ res, app }) {
      if (res.locals && res.locals.settings) {
        state.settings = res.locals.settings;
      }
  
      state.settings["hide_thumbs"] = app.$cookies.get("hide_thumbs")
      state.settings["theme.is_dark"] = app.$cookies.get("theme.is_dark")
    },
    async setSetting(setting) {
      await this.$axios.$post("/settings", setting);
      state.settings[setting.key] = setting.value;
    },
    async setLocalSetting(setting) {
      this.$cookies.set(setting.key, setting.value, {
        path: "/",
        maxAge: 60 * 60 * 24 * 7,
      });
      state.localSettings[setting.key] = setting.value;
    },
  }
})

