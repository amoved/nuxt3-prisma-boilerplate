// https://nuxt.com/docs/api/configuration/nuxt-config

const locales = require('./locales/index')

export default defineNuxtConfig({
  app: {
    head: {
           link: [{ rel: 'icon', type: 'image/svg', href: "./favicon.svg" }]
        }
  },
  devtools: { enabled: true },
  css: [
    'vuetify/lib/styles/main.css',
    '@mdi/font/css/materialdesignicons.min.css'
  ],
  build: {
    transpile: ['vuetify']
  },
  modules: [
    '@pinia/nuxt',
    '@sidebase/nuxt-auth',
    '@nuxtjs/i18n',
    '@vee-validate/nuxt',
    // [
    //   '@nuxtjs/i18n',
    //   {
    //     locales: Object.keys(locales).map(key => ({
    //       code: key,
    //       name: locales[key],
    //       // file: `${key}.json`,
    //       file: 'loader.js',
    //       iso: key
    //     })),
    //     vueI18n: {
    //       fallbackLocale: 'en',
    //       silentTranslationWarn: true
    //     },    
    //     langDir: 'locales',
    //     lazy: true,
    //     strategy: 'no_prefix',
    //     skipSettingLocaleOnNavigate: true,
    //   }
    // ]
  ],
  auth: {   
    globalAppMiddleware: true,     
    provider: {            
      type: 'local'        
    }
  },
  i18n: {
    locales: Object.keys(locales).map(key => ({
      code: key,
      name: locales[key],
      file: `${key}.json`,
      // file: 'loader.js',
      iso: key
    })),
    // vueI18n: {
    //   fallbackLocale: 'en',
    //   silentTranslationWarn: true
    // }, 
    defaultLocale: 'es',
    fallbackLocale: 'es',   
    langDir: 'locales',
    lazy: true,
    strategy: 'no_prefix',
    skipSettingLocaleOnNavigate: true,
    compilation: {
      strictMessage: false,
    },
  },
  runtimeConfig: {
    public: {
      baseURL: process.env.BASE_URL || 'https://localhost:3000/',
    },
  },
})
