# Plantilla de Nuxt 3 + Prisma 

Este repo es una plantilla para tu proyecto con Nuxt3, Vuetify y Prisma.

#### ¿Qué es Nuxt3?

Nuxt.js v3 es la versión más reciente de Nuxt marco de trabajo progresivo para Vue. 

#### ¿Qué es Vuetify?

Una biblioteca de componentes y estilos para Vue.

#### ¿Qué es Prisma?

Un ORM (Object-Relational Mapping) para conectar tu servicio de backend con tu base de datos.

#### ¿Cuándo usar este stack?

Estas son algunas de las funcionalidades que puedes contemplar con estas tecnologías:

- Aplicación fullstack: al compilar el proyecto obtendras tanto un cliente como un servidor. 
- Selección de la base de datos que mejor se adapte a tu proyecto: gracias al uso de Prisma podrás cambiar de proveedor de base de datos sin apenas complicaciones.
- Usuarios

### Arquitectura

Esta plantilla está diseñada de la siguiente manera:

Se han instalado en un paquete de npm tanto Nuxt3 como Prisma. A continuación se ha inyectado Vuetify a Nuxt como un plugin.

### Stack

- TypeScript y JavaScript: lenguajes de programación
- [Vue 3]((https://vuejs.org)): framework de JS para desarrollo frontend
- Nuxt 3: framework de Vue para aplicaciones fullstack
- Vuetify 3: biblioteca de componentes UI
- Material Design Icons: biblioteca de iconos
- Prisma: ORM
- MySQL: base de datos

Utilidades:
- Pug: preprocesador de HTML
- Sass: preprocesador de CSS

Módulos de Nuxt:
- [Pinia](https://pinia.vuejs.org/introduction.html): control de estado de la aplicación
- nuxt-auth (sidebar): facilita la gestión de credenciales en el cliente y permite integrar con OAuth y MagicLinks.
- i18n: soporte multi-idiomas de forma sencilla.
- [vee-validate](https://vee-validate.logaretm.com/v4/integrations/nuxt/): módulo de validación para campos de texto.




---
# Nuxt 3 + Prisma boilerplate (EN)

This repository is intended as a starting point for projects using Nuxt3 and Prisma.


---

Look at the [Nuxt 3 documentation](https://nuxt.com/docs/getting-started/introduction) to learn more.

## Setup

Make sure to install the dependencies:

```bash
# npm
npm install

# pnpm
pnpm install

# yarn
yarn install

# bun
bun install
```

## Development Server

Start the development server on `http://localhost:3000`:

```bash
# npm
npm run dev

# pnpm
pnpm run dev

# yarn
yarn dev

# bun
bun run dev
```

## Production

Build the application for production:

```bash
# npm
npm run build

# pnpm
pnpm run build

# yarn
yarn build

# bun
bun run build
```

Locally preview production build:

```bash
# npm
npm run preview

# pnpm
pnpm run preview

# yarn
yarn preview

# bun
bun run preview
```

Check out the [deployment documentation](https://nuxt.com/docs/getting-started/deployment) for more information.

## Prisma

#### Create a DB and connect to it

Create a database of your choice, here we use SQLite db. Then add its path to the .env file.

Otherwise you can use the container provided in the docker-compose file. Rename .example.env to .env and change the values. Remember to update the DB_URL following the format of your db provider as Prisma accepts it (https://pris.ly/d/connection-strings). Also remember to update the provider at ./prisma/schema.prisma. Then run: 
```bash
docker compose up
```

**Note**: the db volume is configured to be stored in ./containers/db/data change this config if it doesn't fit your needs.


Then access PhpMyAdmin and grant db priviledges to the db user.

<img src="./docs/assets/img/phpmyadmin_add_user_priv.png">

#### Create and seed the database

Run the following command to create your SQLite database file. This also creates the User table that are defined in prisma/schema.prisma:

```bash
npx prisma migrate dev --name initial-migration
```

When npx prisma migrate dev is executed against a newly created database, seeding is also triggered. The seed file in prisma/seed.js will be executed and your database will be populated with the sample data.
