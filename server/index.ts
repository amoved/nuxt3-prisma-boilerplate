import { AuthAdapterBcryptJWT } from './../ddd/shared/infrastructure/adapter.bcryptJwt';
import { UserController } from '../ddd/user/infrastructure/controller'
import { UserRepositoryPrismaMySQL } from '~/ddd/user/infrastructure/repository.mysqlPrisma';
import { UserService } from "~/ddd/user/application/user.service";
import { LoggerAdapterWinston } from '~/ddd/log/infrastructure/adapter.winston';
import { config } from './config';
import { ConfigController } from '../ddd/config/infrastructure/controller';
import { ConfigService } from '../ddd/config/application/config.service';
import { ConfigRepositoryPrismaMySQL } from '../ddd/config/infrastructure/repository.mysqlPrisma';
import { MailerController } from '~/ddd/mailer/infrastructure/controller';
import { MailerService } from '~/ddd/mailer/application/mailer.service';
import type { MailerOptions } from '~/ddd/mailer/domain/ports/mailer.interface';
import { MailerAdapterEmailTemplates } from '~/ddd/mailer/infrastructure/adapter.emailTemplates';

// User service
const loggerPort = new LoggerAdapterWinston(config);
const authPort = new AuthAdapterBcryptJWT();
const userRepository = new UserRepositoryPrismaMySQL(loggerPort);
const userService = new UserService(userRepository, authPort, loggerPort);

export const userController = new UserController(userService)

// Mail service
const mailerOptions: MailerOptions = {
    notifier: {
        fromName: config.appName,
        fromAddress: config.smtp.email,
        locale: config.locale,
    },
    smtpConnection: config.smtp,
}
const mailerPort = new MailerAdapterEmailTemplates(mailerOptions)
const mailerService = new MailerService(mailerOptions,mailerPort)

export const mailerController = new MailerController(mailerService)

// Config service
const configRepository = new ConfigRepositoryPrismaMySQL(loggerPort);
const configService = new ConfigService(config, configRepository, authPort, loggerPort, mailerPort);
// configService.init(config)

export const configController = new ConfigController(configService)

console.log("Controllers created!")