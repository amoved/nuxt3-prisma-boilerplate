import { userController } from "../../index";

export default eventHandler(async (event) => {

  console.log('[api/auth/restore] Set new password')

  const { temporaryAccessToken, password } = await readBody(event);

  const user = await userController.restore(temporaryAccessToken, password)
  console.log(`[api/auth/restore] Password restored for user ${user.id}`)

  return {
    restoredPassword: !!user ? true : false
  }
});
