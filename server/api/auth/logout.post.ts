import { userController } from "../../index"

export default eventHandler(async (event) => {

  const authorization = await getHeader(event, 'authorization')
  const loggedOut = await userController.logout(authorization)

  return {
    loggedOut
  }
});
