import { userController } from "../../index";
import { authController } from "../../index";

export default eventHandler(async (event) => {

  try {

    const authorization = event.headers.get('authorization')
    
    if(authorization) {

      // Validate input
      // const authorization = useRequestHeaders(['authorization'])
      console.log(`Trying to get session, auth: ${authorization}`)
      
      // Validate token and get user 
      const user = await userController.session(authorization)
      
      // Return session   
      return {
        user
      }
    }
  }
  // }
  catch(e){
    console.log(e)
    return false
  }
});
