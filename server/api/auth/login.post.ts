import { userController } from "../../index";

export default eventHandler(async (event) => {
  // Validate input
  const { email, password } = await readBody(event);
  console.log(`Request to login`)
  console.log(`Received params: ${email}, ${password}`)

  const accessToken = await userController.login(email, password)

  // Return token 
  return {
    token: accessToken
  };
});
