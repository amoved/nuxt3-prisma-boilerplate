import { userController } from "../index";

export default defineEventHandler(async (event) => {

  const { name, email, password } = await readBody(event)

  return userController.register(name, email, password)
})